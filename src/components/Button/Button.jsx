import React from 'react';
import "./Button.css";

export default class Button extends React.Component {
    render() {
        const {optionalClassName, backgroundColor, text, onClick } = this.props;

        return (
            <button
                className={"btn " + optionalClassName}
                style={{ backgroundColor }}
                onClick={onClick}
            >
                {text}
            </button>
        );
    }
}
